# -*- coding: utf-8 -*-
"""Module that uses Starting Position, Current Position
and pre-calculated bearing from SP to Waypoint"""
from math import radians, asin, sin
from pubsub import pub
from pyproj import Geod

import control

settings = control.settings
units = control.units


class Odometer(object):
    """Odometer and Cross Track Error"""

    def __init__(self):
        # pub.subscribe(self.do_odometer, 'lat_lon')
        self.odometer_bearing = None
        self.odometer_distance = None

    def do_odometer(self, lat, lon):
        """Start Point to current position distance
        :param lat:
        :param lon:
        """
        if not settings.show_odometer and not settings.show_crosstrack:
            return

        if lat is None:
            return

        if settings.startpoint_lat is None:
            self.reset_startpoint()  # Does anything, or just get in the way?
            return
        try:
            geoid = Geod(ellps='WGS84')
            startpoint_lat = settings.startpoint_lat
            startpoint_lon = settings.startpoint_lon
            self.odometer_bearing, __, self.odometer_distance = geoid.inv(startpoint_lon, startpoint_lat, lon, lat)

        except Exception as error:
            print "Can't calculate odometer: ", error

        finally:
            if settings.show_odometer:
                odo_distance = self.odometer_distance
                odo_distance *= units.DISTANCE[settings.distance_units]
                pretty_odometer = '{0:.2f} {1}'.format(odo_distance, settings.distance_units)

                pub.sendMessage('show_odometer', show_odometer=pretty_odometer)
                print 'Odometer:', pretty_odometer  # Test

            self.do_crosstrack()  # Call crosstrack()
            return  # TODO: Add unsubscribe

    def do_crosstrack(self):
        """
        http://williams.best.vwh.net/avform.htm#XTE

        Cross track error:

        Suppose you are proceeding on a great circle route from
        A to B (course =crs_AB) and end up at D, perhaps off course.
        (We presume that A is ot a pole!) You can calculate the
        course from A to D (crs_AD) and the original_distance from A to
        D (dist_AD) using the formulae above. In terms of these the cross track
        error, XTD, (distance off course) is given by
         crosstrack_distance = asin(sin(distancetravelled) * sin(coursetaken - courseintended))
         XTD =asin(sin(dist_AD)*sin(crs_AD-crs_AB))

        (positive XTD means right of course, negative means left)
        (If the point A is the N. or S. Pole replace crs_AD-crs_AB with
        lon_D-lon_B or lon_B-lon_D, respectively.)

        The "along track distance", ATD, the distance from A along the
        course towards B to the point abeam D is given by:

                 ATD=acos(cos(dist_AD)/cos(XTD))

        For very short distances:

                 ATD=asin(sqrt( (sin(dist_AD))^2 - (sin(XTD))^2 )/cos(XTD))

        is less susceptible to rounding error

        Note that we can also use the above formulae to find the point of
        closest approach to the point D on the great circle through A and B

        Formula:
        self.original_distance = 'distance' from StartPoint to Current Position
        self.original_bearing = 'bearing' from StartPoint to Current Position
        settings.startpoint_to_waypoint_bearing = 'origc' bearing from StartPoint to Waypoint
        'earth_radius' is the earth’s radius...it's big.

        crosstrack_distance =asin(sin(original_distance)*sin(bearing - origc)) * earth_radius

        (positive crosstrack_distance  means right of course, negative means left...we hope.)
        """
        if not settings.show_crosstrack:
            return

        if not settings.waypoint_lat:  # Should be unneccessary
            print 'Waypoint has not been set'  # TODO: Open waypoint dialogue
            return

        if not settings.startpoint_to_waypoint_bearing:
            return

        pretty_crosstrack = None

        try:
            startpoint_to_waypoint_bearing = radians(settings.startpoint_to_waypoint_bearing)

            earth_radius = 6371009.0  # meters...It's a mean radius, but nice enough.
            odobearing = radians(self.odometer_bearing)
            crosstrack_distance = (asin(sin(settings.startpoint_to_waypoint_distance) * sin(odobearing - startpoint_to_waypoint_bearing)) * earth_radius)

            crosstrack_distance *= units.DISTANCE[settings.distance_units]
            pretty_crosstrack = '{0:2.1f} {1}'.format(crosstrack_distance, settings.distance_units)

        except Exception as error:
            print 'Can\'t calculate crosstrack because: ', error
            pretty_crosstrack = 'There be dragons.'

        finally:
            pub.sendMessage('crosstrack', show_crosstrack=pretty_crosstrack)
            print 'Cross Track: ', pretty_crosstrack  # Test
            return

    @staticmethod
    def reset_startpoint():
        """set to None and re-fetch"""
        settings.startpoint_lat = None
        settings.startpoint_lon = None
        settings.startpoint_to_waypoint_bearing = None
        settings.startpoint_to_waypoint_distance = None
        return


odometer = Odometer()
