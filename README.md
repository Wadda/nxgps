
#NxGPS - The Waddometer
###Getting the most out of a $30 GPS

Files in this repository:

**run.py** Yoke for all modules.

**control.py** User defaults and configuraton.

**gps.py** Modified from GPSD Project (/usr/lib/python2.7/dist-packages/gps/gps.py).  Must be moved back.

**gpsclient.py** Streams and publishes data until it doesn't.

**latlon.py** Lat/Lon in 'press ready' DDD; DMM; or DMS form.

**odometer.py** Calculates distance/bearing from starting point and crosstrack error if destination is set.

**sogcog.py** Data smoothing and press ready current, minumum, average, maximum speed; True/Magnetic course.



**timestaus.py** Time/Date (UTC or local) in various formats and mode/satellite information

**waddometer.fbp** WXFormbuilder source code for GUI.



**waypoint.py** Waypoint distance/bearing; VMG; ETA; and Best Scenario (BS™) calculations.

####Required packages

**geomag**

**pypubsub**

**pyproj**

**python-dateutil**

``sudo pip install geomag; sudo pip install pypubsub; sudo pip install pyproj; sudo pip install python-dateutil``
or
``sudo pip install geomag pypubsub pyproj python-dateutil``

Until the gui is running user settings are modified in control.py
