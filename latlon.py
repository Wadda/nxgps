#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Deals with Lat Lon stuff"""
from math import modf, radians

from pubsub import pub
import geomag
import control
from odometer import odometer
from waypoint import waypoint

settings = control.settings
units = control.units


class DoLatLon(object):
    """
    Subscribes to gpsclient.py Lat/Lon data and publishes it according to control.py
    """

    def __init__(self):
        """ do_lat_lon creates lat/lon attributes
        """
        pub.subscribe(self.do_lat_lon, 'lat_lon')

        self.lat = None
        self.lon = None

    def do_lat_lon(self, lat, lon):
        """
        unpacks pubsub message ('lat_lon') from gpsclient
        Sets startpoint if none
        and calls showlatlon if True.

        :param lat:
        :param lon:
        """
        if lat is None:  # One is not without the other...unless it's broken.
            self.lat = self.lon = None
            return

        self.lat = round(lat, 6)  # 6 is enough
        self.lon = round(lon, 6)
        self.showlatlon()
        odometer.do_odometer(self.lat, self.lon)
        if settings.startpoint_lat is None:
            settings.startpoint_lat = self.lat
            settings.startpoint_lon = self.lon
            waypoint.do_bearing_distance(self.lat, self.lon)
            return

        else:

            waypoint.do_bearing_distance(self.lat, self.lon)

        return

    def showlatlon(self):
        """
        returns string in fmt DDD or DMM or DMS
        having been rounded to less than 1 foot @ equator
        """
        pretty_lat = pretty_lon = None
        if not settings.show_latlon:
            return

        if self.lat is None:  # Does it ever do this?
            pretty_lat = pretty_lon = "No Data"
            print 'No_Lat:', pretty_lat, ' No_Lon:', pretty_lon  # Test

        if settings.latlon_units == 'DDD':
            # Latitude
            _lat = abs(self.lat)
            if self.lat > 0.0:
                pretty_lat = '{0:3.6f}° N'.format(_lat)
            else:
                pretty_lat = '{0:3.6f}° S'.format(_lat)

            # Longitude
            _lon = abs(self.lon)
            if self.lon > 0.0:
                pretty_lon = '{0:3.6f}° E'.format(_lon)
            else:
                pretty_lon = '{0:3.6f}° W'.format(_lon)

        if settings.latlon_units == 'DMM':
            # Latitude
            _lat = abs(self.lat)
            minlat, deglat = modf(_lat)
            minlat *= 60
            if self.lat > 0.0:
                pretty_lat = '{0}° {1:2.5f}\' N'.format(int(deglat), minlat)
            else:
                pretty_lat = '{0}° {1:2.5f}\' S'.format(int(deglat), minlat)

            # Longitude
            _lon = abs(self.lon)
            minlon, deglon = modf(_lon)
            minlon *= 60
            if self.lon > 0.0:
                pretty_lon = '{0}° {1:2.4f}\' E'.format(int(deglon), minlon)
            else:
                pretty_lon = '{0}° {1:2.4f}\' W'.format(int(deglon), minlon)

        if settings.latlon_units == 'DMS':
            # Latitude
            _lat = abs(self.lat)
            mmm, deglat = modf(_lat)
            sec, minlat = modf(mmm * 60)
            sec *= 60.0
            if self.lat > 0.0:
                pretty_lat = '{0}° {1}\' {2:2.3f}\" N'.format(int(deglat), int(minlat), sec)
            else:
                pretty_lat = '{0}° {1}\' {2:2.3f}\" S'.format(int(deglat), int(minlat), sec)

            # Longitude
            _lon = abs(self.lon)
            mmm, deglon = modf(_lon)
            sec, minlon = modf(mmm * 60)
            sec *= 60.0
            if self.lon > 0.0:
                pretty_lon = '{0}° {1}\' {2:2.3f}\" E'.format(int(deglon), int(minlon), sec)
            else:
                pretty_lon = '{0}° {1}\' {2:2.3f}\" W'.format(int(deglat), int(minlon), sec)

        pub.sendMessage('show_latlon', show_lat=pretty_lat, show_lon=pretty_lon)
        print 'Latitude: ', pretty_lat, ' Lon: ', pretty_lon  # Test
        return

    def magdec(self):
        """
        Magnetic Declination to write magdec to settings
        Called on configuration write and verifies once a day
        Called with every configuration file write and ever 24 hours
        """
        if settings.true_or_magnetic is True:
            settings.magnetic_declination = 0.0  # Declination in radians)
            settings.magnetic_declination_degrees = 0.0
            return

        if self.lat:
            # That is, if there's latitude and longitude
            try:
                # Not imported if stays True.

                magnetic_declination_degrees = geomag.declination(self.lat, self.lon)
                mag_dec_radians = radians(magnetic_declination_degrees)  # radians
                settings.magnetic_declination = mag_dec_radians
                settings.magnetic_declination_degrees = magnetic_declination_degrees
                settings.write_configuration()

            except Exception as error:
                print 'Magnetic Declination is sick: ', error
                # settings.magnetic_declination = 0.0
                # settings.true_or_magnetic = True
        return


do_latlon = DoLatLon()
