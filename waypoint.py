#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Waypoint distance, bearing, VMG, ETA, Best Scenario (BS)"""

#import commands
#import os
from math import degrees, cos, radians, sin

from pubsub import pub
from pyproj import Geod
from dateutil.relativedelta import *
import control
from sogcog import speedtrack
# from control import settings
from timestatus import timestatus

settings = control.settings
units = control.units


class Waypoint(object):
    """ALL things Waypoint-ish
    Waypoint Bearing
    waypoint Distance
    Distance With Course
    ETA
    VMG
    """

    def __init__(self):
        self.course_delta = None
        self.waypoint_bearing = None  # degrees
        self.waypoint_distance = None  # meters
        self.waypoint_distance_with_course = None  # meters
        # self.ultimate_bearing = None  # No home, yet

    def do_bearing_distance(self, lat, lon):
        """
        Creates
            self.course_delta
            self.waypoint_bearing
            self.waypoint_distance
        Sets
             control.settings.startpoint_to_waypoint_bearing = self.waypoint_bearing
             control.settings.startpoint_to_waypoint_distance = self.waypoint_distance
        and calls
            show_waypoint_bearing
            show_waypoint_distance
        to publish them if True.
        """
        if not settings.show_waypoint_bearing and not settings.show_waypoint_distance and not \
                settings.show_waypoint_distance_with_course and not settings.show_eta and not \
                settings.show_vmg:
            return
        if not settings.waypoint_lat:
            print 'No waypoint to go to.......'  # TODO: Open waypoint dialog
            return

        try:
            geoid = Geod(ellps='WGS84')
            waypoint_lat = settings.waypoint_lat
            waypoint_lon = settings.waypoint_lon

            self.waypoint_bearing, __, self.waypoint_distance = geoid.inv(lon, lat, waypoint_lon, waypoint_lat)

            if not settings.startpoint_to_waypoint_bearing:
                settings.startpoint_to_waypoint_bearing = self.waypoint_bearing
                settings.startpoint_to_waypoint_distance = self.waypoint_distance
                # return  # TODO: return?
            self.vmg()
            self.distance_with_course()
            self.show_waypoint_distance()  # meters
            self.show_waypoint_bearing()  # degrees
            self.eta()
        except ValueError as error:
            print 'do waypoint bearing distance error:', error
            # settings.waypoint_lat = None
            # settings.waypoint_lon = None
            # return

        return

    def vmg(self):
        """
        VMG = cos(course delta) * smoothed_sog
        """
        # pretty_vmg = 'No Data'

        if not settings.show_vmg:
            print 'VMG is not displayed'
            return
        if not speedtrack.cog_smoothed:
            return
        if not self.waypoint_bearing:
            return
        if not self.course_delta:
            self.course_delta = radians(self.waypoint_bearing) - speedtrack.cog_smoothed
            return

        else:
            vmg = cos(self.course_delta) * speedtrack.sog_smoothed

            pretty_vmg = vmg * units.SPEED[settings.speed_units]
            pretty_vmg = '{0:.2f} {1}'.format(pretty_vmg, settings.speed_units)

        pub.sendMessage('show_vmg', show_vmg=pretty_vmg)
        print 'VMG: ', pretty_vmg, 'vmg: ', vmg
        return


    def distance_with_course(self):
        """
        The rational is nothing is shorter than a straight line (arc on a
        geoid); the (arc) the sine and cosine of the distance is the penalty for course delta
        --until 90 degrees abeam...at least when the world is flat.
        """

        if not (settings.show_waypoint_distance_with_course or settings.show_eta):
            return

        if not (self.waypoint_bearing and self.waypoint_distance and speedtrack.cog_smoothed):
            return

        else:
            waypoint_radians = radians(self.waypoint_bearing)
            course_delta = abs(speedtrack.cog_smoothed - waypoint_radians)
            self.waypoint_distance_with_course = (cos(course_delta) + sin(course_delta)) * self.waypoint_distance

            if settings.show_waypoint_distance_with_course:
                distance_with_course = self.waypoint_distance_with_course * units.DISTANCE[
                    settings.distance_units]
                pretty_distance_with_course = '{0:.2f} {1}'.format(distance_with_course,
                                                                   settings.distance_units)

                pub.sendMessage('show_distancepresentcourse',
                                show_distancepresentcourse=pretty_distance_with_course)
                print 'distance_with_course : ', pretty_distance_with_course, 'difference: ', \
                    self.waypoint_distance_with_course - self.waypoint_distance # Test
                return

    def show_waypoint_distance(self):
        if not settings.show_waypoint_distance:
            return
        else:
            pretty_waypoint_distance = self.waypoint_distance * \
                                       units.DISTANCE[settings.distance_units]
            pretty_waypoint_distance = '{0:.2f} {1}'.format(pretty_waypoint_distance, settings.distance_units)

            pub.sendMessage('show_waypoint_distance', show_waypoint_distance=pretty_waypoint_distance)
            print 'Waypoint Distance:', pretty_waypoint_distance  # Test
            return

    def show_waypoint_bearing(self):
        if not settings.show_waypoint_bearing:
            return

        else:
            if settings.true_or_magnetic:
                true_magnetic = 'T'
            else:
                true_magnetic = 'M'

            waypoint_radians = radians(self.waypoint_bearing)
            waypoint_tm = waypoint_radians + settings.magnetic_declination  # Adds 0.0 if True
            bearing_deg = degrees(waypoint_tm)
            bearing_deg %= 360
            cardinal_index = int((bearing_deg + 11.25) // 22.5)
            cardinal_index = abs(cardinal_index)

            __, direction = units.CARDINAL[cardinal_index]
            pretty_waypoint_bearing = '{0:.0f}{1}° {2} '.format(bearing_deg, true_magnetic, direction)

            pub.sendMessage('show_waypoint_bearing', show_waypoint_bearing=pretty_waypoint_bearing)
            print 'WP Bearing: {0} with {1:.0f}° Declination'.format(pretty_waypoint_bearing, degrees(settings.magnetic_declination))  # Test
            return

    def eta(self):
        """
            ETA = distance with present course / smoothed sog
        """
        if not settings.show_eta:
            print 'ETA is not diplayed'
            return

        if not speedtrack.sog_smoothed:
            return

        if not self.waypoint_distance_with_course:
            self.distance_with_course()
            return

        try:
            distance = self.waypoint_distance_with_course
            eta = distance / speedtrack.sog_smoothed
        except ZeroDivisionError:
            pretty_eta = "In your dreams"
            pub.sendMessage('show_eta', show_eta=pretty_eta)

            print 'ETA : ', pretty_eta, 'Real :', distance / speedtrack.sog_smoothed  # Test
            return
        finally:
            if not settings.eta_relative:
                time_delta = eta
                #Convert to days, hours, minutes and seconds
                days, seconds = divmod(time_delta, 24 * 60 * 60)
                hours, seconds = divmod(seconds, 60 * 60)
                minutes, seconds = divmod(seconds, 60)

                absolute_eta = timestatus.gpstime + relativedelta(days=days, hours=hours, minutes=minutes,
                                                                  seconds=seconds)  # or whatever increment it is in.
                pretty_eta = '{0} in some form'.format(absolute_eta)  # TODO Absolute/Relative Time
            else:
                return
                # pretty_eta = '{0} in some unknown unit'.format(eta)  # TODO
            pub.sendMessage('show_eta', show_eta=pretty_eta)
            print 'ETA : ', pretty_eta, 'Real :', distance / speedtrack.sog_smoothed  # Test
            return


waypoint = Waypoint()
