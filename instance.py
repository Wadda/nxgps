#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'wadda'
# Pinched 2014
# Created and re-created many times by a lot of different people over the last 14 years.

import os
import sys
from tempfile import gettempdir


class singleInstance(object):
    def __init__(self):
        self.lockfile = os.path.normpath(gettempdir() + '/' + 'wadda.lock')
        print self.lockfile
        self.single = True
        import fcntl

        self.fd = open(self.lockfile, 'w')
        try:
            fcntl.lockf(self.fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
            return
        except IOError:
            self.single = False
            sys.exit('NxGPS is already running')


if __name__ == '__main__':
    from time import sleep

    lock = singleInstance()
    if lock.single:
        print 'single instance... running'
        sleep(30)
    else:
        print 'another instance is running... quitting'
