#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Create and read user configuration file and settings and periodic functions"""
import ConfigParser
import os
from datetime import timedelta
from time import time
import sys


class Settings(object):
    """
    control.setting for GUI and operational defaults
    """

    def __init__(self):
        # Information
        self.work_directory = os.getcwd()
        self.home_directory = os.path.expanduser('~')
        # self.waddapoints = (self.work_directory + '/bs/waddapoints')
        self.configuration_file = (self.home_directory + '/.wadda/nxgps.configuration')
        self.waypoint_directory = (self.home_directory + '/.wadda/waypoints')
        self.opencpn_xml = (self.home_directory + '/.opencpn/navobj.xml')

        self.version = '0.betty'
        self.termsAgreed = True  # TODO: Change when ready

        # Latitude Longitude
        self.show_latlon = True
        self.latlon_units = 'DMS'  # DDD DMM or DMS
        # self.need_lat_lon_radians = True  # Need?

        # Odometer
        self.show_crosstrack = True
        self.show_odometer = True
        self.distance_units = 'Meters'  # "NM", "Miles", "Kms", "Feet", "Meters"

        # Speed Course
        self.show_course = True
        self.magnetic_declination = 0.0
        self.magnetic_declination_degrees = 0.0
        self.true_or_magnetic = False  # True is True, False is Magnetic
        self.show_minmax = True
        self.show_speed = True
        self.speed_units = 'KTS'  # "KTS", "MPH", "KPH", "M/S"
        self.smoothing = 2  # u"10 Second  Data Smoothing" # 0, 5, 10, 15, 20

        # Startpoint
        # TODO: Replace startpoint latlon with None, None
        # (-33.824488, 151.285293)  #Heads
        self.startpoint_lat = -34.725675  # sydney# (-15.560615, -146.241122) # Apataki Carenage
        self.startpoint_lon = 151.157152  # sydney# (-15.560615, -146.241122) # Apataki Carenage
        self.startpoint_to_waypoint_bearing = float('Inf')  # 0.0001
        self.startpoint_to_waypoint_distance = float('Inf')  # 221834.97029250953

        # Time Status
        self.show_date = True
        self.date_units = 1  # "DD-MM-YY" "MM-DD-YY" "Month, DD YYYY" "Weekday, Month DD, YYYY"
        self.show_time = True
        self.time_units = 4  # '12H:M:S'  # 'UTC', '24Hour', '24H:M:S', '12Hour', '12H:M:S'
        self.timezone = None  # Not in configuration file
        self.timezone_offset = timedelta()  # UTC
        self.show_elapsed = True  # TODO: finish
        self.time_beginning = time()  # TODO: finish
        self.time_relative = True  # Time is Relative; False is Absolute  TODO: Delete?

        # Waypoint
        self.show_eta = True
        self.eta_relative = True  # TODO this or relative?
        self.show_vmg = True
        self.show_waypoint_distance = True
        self.show_waypoint_bearing = True
        self.show_waypoint_distance_with_course = True
        # TODO: Replace waypoint_lat ..._lon with None, None

        self.waypoint_lat = -32.725675  # 8.918333  # Las Brisas # -15.560615 # Apataki Carenage
        self.waypoint_lon = 151.157162  # -79.531017  # Las Brisas # -146.241122 # Apataki Carenage

        # Appearance
        self.bgcolor = (0, 0, 48)  # Navigatrix blue background
        self.colormode = 'Day'
        self.daycolor = (255, 255, 0)  # Canary Yellow
        self.nightcolor = (255, 255, 100)  # Canary Yellow subdued
        self.fontface = 'Arial'
        self.labelfontface = 'smallfonts'
        self.datafont_size = 23
        self.data_default_size = 23  # default size for reset, does not change
        self.labelfont_size = 8
        self.label_default_size = 8  # default size for reset, does not change
        self.screenposition = (0, 0)

    def read_configuration(self):
        """
        Reads configuration settings that should be at
        ~/.wadda/nxgps.configuration
        """
        # datetime.strptime('2014-04-25', '%Y-%m-%d')  # dummy call  http://bugs.python.org/issue7980 Doesn't work here.

        if os.path.isfile(self.configuration_file):
            print 'reading configuration from: ', self.configuration_file
            configuration = ConfigParser.RawConfigParser()
            configuration.read(self.configuration_file)

            try:
                # information
                self.termsAgreed = configuration.get('NxGPS', 'termsAgreed')
                self.waypoint_directory = configuration.get('NxGPS', 'waypoint_directory')

                # Latitude Longitude
                self.show_latlon = configuration.getboolean('Latitude Longitude', 'show_latlon')
                self.latlon_units = configuration.get('Latitude Longitude', 'latlon_units')

                # Odometer
                self.show_crosstrack = configuration.getboolean('Odometer', 'show_crosstrack')
                self.show_odometer = configuration.getboolean('Odometer', 'show_odometer')
                self.distance_units = configuration.get('Odometer', 'distance_units')

                # Speed Course
                self.show_course = configuration.getboolean('Speed Course', 'show_course')
                self.magnetic_declination = configuration.getfloat('Speed Course', 'magnetic_declination')
                self.magnetic_declination_degrees = configuration.getfloat('Speed Course', 'magnetic_declination_degrees')
                self.true_or_magnetic = configuration.getboolean('Speed Course', 'true_or_magnetic')
                self.show_minmax = configuration.getboolean('Speed Course', 'show_minmax')
                self.show_speed = configuration.getboolean('Speed Course', 'show_speed')
                self.speed_units = configuration.get('Speed Course', 'speed_units')
                self.smoothing = configuration.get('Speed Course', 'smoothing')

                # Startpoint
                self.startpoint_lat = configuration.getfloat('Startpoint', 'startpoint_lat')
                self.startpoint_lon = configuration.getfloat('Startpoint', 'startpoint_lon')
                self.startpoint_to_waypoint_bearing = configuration.getfloat('Startpoint', 'startpoint_to_waypoint_bearing')
                self.startpoint_to_waypoint_distance = configuration.getfloat('Startpoint', 'startpoint_to_waypoint_distance')

                # Time Status
                self.show_date = configuration.getboolean('Time Status', 'show_date')
                self.date_units = configuration.getint('Time Status', 'date_units')
                self.show_time = configuration.getboolean('Time Status', 'show_time')
                self.time_units = configuration.getint('Time Status', 'time_units')
                self.show_elapsed = configuration.getboolean('Time Status', 'show_elapsed')
                self.time_beginning = configuration.getfloat('Time Status', 'time_beginning')
                self.time_relative = configuration.getboolean('Time Status', 'time_relative')

                # Waypoint
                self.show_eta = configuration.getboolean('Waypoint', 'show_eta')
                self.eta_relative = configuration.getboolean('Waypoint', 'eta_relative')
                self.show_vmg = configuration.getboolean('Waypoint', 'show_vmg')
                self.show_waypoint_distance = configuration.getboolean('Waypoint', 'show_waypoint_distance')
                self.show_waypoint_bearing = configuration.getboolean('Waypoint', 'show_waypoint_bearing')
                self.show_waypoint_distance_with_course = configuration.getboolean('Waypoint',
                                                                                   'show_waypoint_distance_with_course')
                self.waypoint_lat = configuration.getfloat('Waypoint', 'waypoint_lat')
                self.waypoint_lon = configuration.getfloat('Waypoint', 'waypoint_lon')

                # appearance
                self.screenposition = configuration.get('Appearance', 'screenposition')

                x = configuration.getint('Appearance', 'screenx')
                y = configuration.getint('Appearance', 'screeny')
                self.screenposition = (x, y)

                self.fontface = configuration.get('Appearance', 'fontface')

                self.datafont_size = configuration.getint('Appearance', 'datafont_size')
                # labelfont_size is controlled by datafont_size

                # self.daycolor = configuration.get('Appearance', 'daycolor')
                # self.daycolor = tuple(int(s)
                #                       for s in self.daycolor[-1].split(','))
                # # TODO: Consider Redshift integration
                # self.nightcolor = configuration.get('Appearance', 'nightcolor')
                # self.nightcolor = tuple(int(s)
                #                         for s in self.nightcolor[-1].split(','))

            except ValueError or IOError as error:
                print 'The read configuration error is:', error
                os.makedirs(self.home_directory + '/.wadda/')
        else:
            print '-- writing default configuration file --'
            self.write_configuration()
                #
                # self.write_configuration()
                # retries = 0
                # while IOError and retries < 2:
                #     try:
                #         self.read_configuration()
                #     except IOError:
                #         print 'Error recovery re-read of the configuration file'
                #         return
                #     retries += 1
                # sys.exit('Something is really stuffed.\nCheck ~/.wadda')
        return

    def write_configuration(self):
        """
        Writes configuration settings that should
        be at ~/.wadda/nxgps.configuration
        """
        # magdec()
        # timezone()

        try:
            print 'writing configuration to: ', self.configuration_file  # TODO: Needs exceptions
            configuration = ConfigParser.RawConfigParser()

            # NxGPS
            configuration.add_section('NxGPS')
            configuration.set('NxGPS', 'version', self.version)
            configuration.set('NxGPS', 'termsAgreed', self.termsAgreed)
            configuration.set('NxGPS', 'waypoint_directory', self.waypoint_directory)

            # Latitude Longitude
            configuration.add_section('Latitude Longitude')
            configuration.set('Latitude Longitude', 'show_latlon', self.show_latlon)
            configuration.set('Latitude Longitude', 'latlon_units', self.latlon_units)

            # Odometer
            configuration.add_section('Odometer')
            configuration.set('Odometer', 'show_crosstrack', self.show_crosstrack)
            configuration.set('Odometer', 'show_odometer', self.show_odometer)
            configuration.set('Odometer', 'distance_units', self.distance_units)

            # Speed Course
            configuration.add_section('Speed Course')
            configuration.set('Speed Course', 'show_course', self.show_course)
            configuration.set('Speed Course', 'show_speed', self.show_speed)
            configuration.set('Speed Course', 'show_minmax', self.show_minmax)
            configuration.set('Speed Course', 'speed_units', self.speed_units)
            configuration.set('Speed Course', 'smoothing', self.smoothing)
            configuration.set('Speed Course', 'true_or_magnetic', self.true_or_magnetic)
            configuration.set('Speed Course', 'magnetic_declination', self.magnetic_declination)
            configuration.set('Speed Course', 'magnetic_declination_degrees', self.magnetic_declination_degrees)

            # Startpoint
            configuration.add_section('Startpoint')
            configuration.set('Startpoint', 'startpoint_lat', self.startpoint_lat)
            configuration.set('Startpoint', 'startpoint_lon', self.startpoint_lon)
            configuration.set('Startpoint', 'startpoint_to_waypoint_distance', self.startpoint_to_waypoint_distance)
            configuration.set('Startpoint', 'startpoint_to_waypoint_bearing', self.startpoint_to_waypoint_bearing)

            # Time Status
            configuration.add_section('Time Status')
            configuration.set('Time Status', 'show_date', self.show_date)
            configuration.set('Time Status', 'date_units', self.date_units)
            configuration.set('Time Status', 'show_time', self.show_time)
            configuration.set('Time Status', 'time_units', self.time_units)
            configuration.set('Time Status', 'show_elapsed', self.show_elapsed)
            configuration.set('Time Status', 'time_beginning', self.time_beginning)
            configuration.set('Time Status', 'time_relative', self.time_relative)
            configuration.set('Time Status', 'timezone', self.timezone)

            # Waypoint
            configuration.add_section('Waypoint')
            configuration.set('Waypoint', 'show_eta', self.show_eta)
            configuration.set('Waypoint', 'eta_relative', self.eta_relative)
            configuration.set('Waypoint', 'show_vmg', self.show_vmg)
            configuration.set('Waypoint', 'show_waypoint_distance', self.show_waypoint_distance)
            configuration.set('Waypoint', 'show_waypoint_bearing', self.show_waypoint_bearing)
            configuration.set('Waypoint', 'show_waypoint_distance_with_course', self.show_waypoint_distance_with_course)
            configuration.set('Waypoint', 'waypoint_lat', self.waypoint_lat)
            configuration.set('Waypoint', 'waypoint_lon', self.waypoint_lon)

            # Appearance
            configuration.add_section('Appearance')
            configuration.set('Appearance', 'screenx', self.screenposition[0])
            configuration.set('Appearance', 'screeny', self.screenposition[1])
            configuration.set('Appearance', 'screenposition', self.screenposition)
            configuration.set('Appearance', 'datafont_size', self.datafont_size)
            configuration.set('Appearance', 'fontface', self.fontface)
            configuration.set('Appearance', 'daycolor', self.daycolor)
            configuration.set('Appearance', 'nightcolor', self.nightcolor)

            configure = open(self.configuration_file, 'wb')
            configuration.write(configure)
            configure.close()
        except IOError:
            # TODO: find file location
            os.makedirs(self.home_directory + '/.wadda/')
            os.makedirs(self.waypoint_directory)
            # self.write_configuration()
            # retries = 0
            # if IOError and retries < 2:
            #     try:
            #         self.write_configuration()
            #     except IOError:
            #         print 'Error recovery write of the configuration file'
            #         return
            #     retries += 1
            # sys.exit('Something is really stuffed.\nCheck ~/.wadda')
        return

settings = Settings()


class Units():

    # Units per meter
    DISTANCE = {
        'NM': 0.0005399568,
        'Miles': 0.00062137119,
        'Kms': 0.001,
        'Feet': 3.2808399,
        'Meters': 1.0
    }

    CARDINAL = (
        (011.25, 'N'), (033.75, 'NNE'), (056.25, 'NE'), (078.75, 'ENE'),
        (101.25, 'E'), (123.75, 'ESE'), (146.25, 'SE'), (168.75, 'SSE'),
        (191.25, 'S'), (213.75, 'SSW'), (236.25, 'SW'), (258.75, 'WSW'),
        (281.25, 'W'), (303.75, 'WNW'), (326.25, 'NW'), (348.75, 'NNW'),
        (360, 'N')
    )
    # CARD = {011.25: 'N', 033.75: 'NNE', 056.25: 'NE', 078.75: 'ENE',
    #         101.25: 'E', 123.75: 'ESE', 146.25: 'SE', 168.75: 'SSE',
    #         191.25: 'S', 213.75: 'SSW', 236.25: 'SW', 258.75: 'WSW',
    #         281.25: 'W', 303.75: 'WNW', 326.25: 'NW', 348.75: 'NNW',
    #         360: 'N'}
    #

    # Units per meter/seconds
    SPEED = {
        'KTS': 1.9438445,
        'MPH': 2.2369363,
        'KPH': 3.6,
        'M/S': 1
    }

units = Units()
