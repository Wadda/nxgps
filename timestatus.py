#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Time stuff"""
from time import time
from datetime import datetime
from pubsub import pub
import dateutil.parser
import dateutil.tz
from dateutil.relativedelta import relativedelta

import control
import latlon

settings = control.settings
units = control.units


class TimeStatus(object):
    """This class deals with *time* related functions of the gps and send them
    to a GUI or other module as a human formatted object.  Time rolls off the
    gps through gpsdclient.py in two formats:
                    1- UTC = u'2013-10-26T12:39:07.000Z'
                    2- Time= 1382791147.0
    coming from the gpsdclient as:
                    pub.sendMessage('UTC', utc=gpsd.utc)
                    pub.sendMessage('Time', gps_time = gpsd.fix.time)
    The 'Time' message is not sent from gpsclient presently and this module just
    receives the 'UTC' message.
    """

    def __init__(self):
        self.today = None
        self.gpstime = None
        pub.subscribe(self.do_time_and_date, 'utc')

    def do_time_and_date(self, utc):
        """
        unpacks gpsclient pub.sendMessage('utc', utc=data.utc)
        and calls relevant functions if True.
        Validates timezone and magnetic declination once every 24 hours.
        """
        if not utc:
            return
        else:
            self.gpstime = dateutil.parser.parse(utc)  # Become datetime object
            print type(self.today), type(self.gpstime)
            if not self.today or self.today <= self.gpstime:  # So we can do this comparison
                latlon.do_latlon.magdec()  # TODO: Remember to add ui events for this function
                deltatee = dateutil.tz.tzlocal()
                settings.timezone = deltatee.tzname(self.gpstime)
                settings.timezone_offset = deltatee.utcoffset(self.gpstime)
                self.today = self.gpstime + relativedelta(days=1)

        self.showtime()
        self.showdate()
        misc.showelapsed()
        return

    def showtime(self):
        """Take UTC time output from gps in ISO 8601 into other formats
        according to user settings.
              # 'UTC', '24Hour', '24H:M:S', '12H:M AMPM', '12H:M:S TZ', '12H:M:S TZ'
        """
        if not settings.show_time:
            return

        pretty_time = None

        if self.gpstime is None:  # Shouldn't ever happen (unnecessary?)
            pretty_time = "No Data"
            pub.sendMessage('show_time', show_time=pretty_time)
            print 'ND Time: ', pretty_time
            return

        shiptime = self.gpstime

        try:
            if settings.time_units == 0:  # UTC
                pretty_time = shiptime.strftime('%H:%M:%S '), ' UTC'

            elif settings.time_units == 1:  # '24Hour':
                shiptime += settings.timezone_offset  # Add TimeZone Offset
                pretty_time = shiptime.strftime('%H:%M'), ' ', settings.timezone

            elif settings.time_units == 2:  # '24H:M:S':
                shiptime += settings.timezone_offset  # Add TimeZone Offset
                pretty_time = shiptime.strftime('%H:%M:%S '), ' ', settings.timezone

            elif settings.time_units == 3:  # '12Hour':
                shiptime += settings.timezone_offset  # Add TimeZone Offset
                pretty_time = shiptime.strftime('%I:%M%p').lstrip('0')

            elif settings.time_units == 4:  # '12H:M:S':
                shiptime += settings.timezone_offset  # Add TimeZone Offset
                assert isinstance(shiptime.strftime('%I:%M:%S').lstrip, object)
                pretty_time = shiptime.strftime('%I:%M:%S').lstrip('0') + ' ' + settings.timezone

        except Exception as error:
            pretty_time = error

        finally:
            pub.sendMessage('show_time', show_time=pretty_time)
            print 'Time: ', pretty_time  # Test

        return

    def showdate(self):
        """
        Converts datetime.datetime object to 'pretty' format
        """
        if not settings.show_date:
            return

        pretty_date = None

        if self.gpstime is None:  # Shouldn't ever happen...should it.
            pretty_date = "Ooops"
            pub.sendMessage('show_date', show_date=pretty_date)
            print 'Date: ', pretty_date  # Test
            return

        shiptime = self.gpstime

        try:
            if settings.time_units != 0:  # "UTC":
                shiptime += settings.timezone_offset

            if settings.date_units == 0:  # "DD-MM-YY":
                pretty_date = shiptime.strftime('%d-%m-%y')

            elif settings.date_units == 1:  # "MM-DD-YY":
                pretty_date = shiptime.strftime('%m-%d-%y')

            elif settings.date_units == 2:  # "Month DD, YYYY":
                pretty_date = shiptime.strftime('%b %d, %Y')

            elif settings.date_units == 3:  # "Weekday, Month DD, YYYY":
                pretty_date = shiptime.strftime('%A, %b %d, %Y')

        except Exception as error:
            pretty_date = error
            print 'Date error :', error

        finally:
            pub.sendMessage('show_date', show_date=pretty_date)
            print 'Date: ', pretty_date
            return


class Status(object):
    """
    GPS Fix Mode
    """
    def __init__(self):
        """
        unpacks gps client pub.sendMessage('satstats', mode=self.fix.mode, usedinview=self.satellites)
        data.fix.mode       # 1|2|3 (1:no fix; 2:2D; 3:3D)
        and calls relevant functions
        """
        pub.subscribe(self.do_satstats, 'satstats')

    @staticmethod
    def do_satstats(mode, usedinview):
        pretty_mode = pretty_sats = 'No Data'
        sats_used, sats_inview = usedinview

        if mode is None:
            pretty_mode = 'No GPS Signal'
            pretty_sats = '~ of ~'

        elif mode == 1:
            pretty_mode = 'No Fix'
            pretty_sats = '~ of ~'

        elif mode == 2:
            pretty_mode = '2D Fix'
            pretty_sats = '{} of {}'.format(sats_used, sats_inview)

        elif mode == 3:
            pretty_mode = '3D Fix'
            pretty_sats = '{} of {}'.format(sats_used, sats_inview)

        pub.sendMessage('show_quality', show_mode=pretty_mode, show_usedinview=pretty_sats)
        print 'Quality: ', pretty_mode, 'using', pretty_sats, 'satellites in view'  # Test
        return

timestatus = TimeStatus()
status = Status()


class Misc(object):
    """..."""
    def __init__(self):
        return

    @staticmethod
    def showelapsed():
        if not settings.show_elapsed:
            return

        pretty_elapsed = None
        try:
            time_now = time()
            time_delta = time_now - settings.time_beginning

            #Convert to days, hours, minutes and seconds
            days, seconds = divmod(time_delta, 24 * 60 * 60)
            hours, seconds = divmod(seconds, 60 * 60)
            minutes, seconds = divmod(seconds, 60)

            if time_delta > 86400:
                pretty_elapsed = '{0:.0f} Day {1:02.0f}:{2:02.0f}:{3:02.0f}'.format(days, hours, minutes, seconds)
            elif 3600 < time_delta < 86400:
                pretty_elapsed = '{0:.0f}:{1:02.0f}:{2:02.0f}'.format(hours, minutes, seconds)
            elif 60 < time_delta < 3600:
                pretty_elapsed = '{0:.0f}:{1:02.0f}'.format(minutes, seconds)
            elif time_delta < 60:
                pretty_elapsed = '{0:.0f} seconds'.format(seconds)

        except Exception as error:
            pretty_elapsed = error

        finally:
            pub.sendMessage('show_elapsed', show_elapsed=pretty_elapsed)
            print 'Elapsed Time: ', pretty_elapsed, 'timedelta: ', time_delta

            return

    def timer_go(self):
        """
        spins time_beginning until a latch signal"""
        pass

    def timer_stop(self):
        """"
        freezes time_now
        """
        pass

    def timer_pause(self):
        """
        store time off and subtract from time equation
        """
        pass

    def timer_reset(self):
        settings.time_beginning = time()
        return

misc = Misc()
