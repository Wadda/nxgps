#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""sogcog.py listens for speed/track data;
produces smoothed, formatted speed and course. """
__author__ = 'wadda'
from cmath import rect, polar
from collections import deque
from math import radians, degrees

from pubsub import pub
from control import settings, units


SMOOTHQUE = deque(maxlen=settings.smoothing * 5)  # The GUI list index is the increment * 5
AVERAGEQUE = deque(maxlen=3600)  # 1 hour running average
MINAVGMAX = [0.0, 0.0, 0.0]


class SpeedTrack(object):
    """
    Execute Speed/Course calculations
    """

    def __init__(self):
        pub.subscribe(self.sogcog_smoother, 'speed_track')
        self.sog_smoothed = self.cog_smoothed = None

    def sogcog_smoother(self, sog, cog):
        """
        Speed Over Ground and track data smoothed over user selected period
        Returns Speed Over Ground...in meters and Course Over Ground in radians
        data smoothed over 0, 5, 10, 15, 20 seconds via control.settings.py selected
        intervals as a basis for all other speed/course influenced calculations.
        """
        if not cog:
            return

        try:
            cog_radian = radians(cog)  # # Need radians for calculations
            cartesian_sog_cog = rect(sog, cog_radian)  # Create complex number 'sog_cog'

            SMOOTHQUE.append(cartesian_sog_cog)
            # reduce(lambda a, b: a+b,SMOOTHQUE) == (((((a + b) + c) + d + ...) + z)
            sog_cog_sum = reduce(lambda a, b: a + b, SMOOTHQUE)
            sog_cog_average = sog_cog_sum / len(SMOOTHQUE)

            # Un-create the complex number
            self.sog_smoothed, self.cog_smoothed = polar(sog_cog_average)  # sog in meters; cog is in radians.

        except Exception as error:
            print 'This Speed/Course error: ', error, "shouldn't happen."
            return

        finally:
            self.showspeed()
            self.showcourse()
            self.showminmax()

            return

    def showspeed(self):
        """
        returns string in control.settings.speed_units rounded to the 100th because knots are to blame.
        """
        if not settings.show_speed:
            return

        if not self.sog_smoothed:
            pretty_sog = "No Data"

        else:
            pretty_sog = self.sog_smoothed
            pretty_sog *= units.SPEED[settings.speed_units]
            pretty_sog = '{0:2.2f} {1}'.format(pretty_sog, settings.speed_units)

        pub.sendMessage('show_speed', show_speed=pretty_sog)
        print 'Speed: ', pretty_sog  # Test
        return

    def showcourse(self):
        """ALL things Course
        """
        if not settings.show_course:
            return

        if not self.cog_smoothed:
            pretty_cog = "No Data"

        else:
            if settings.true_or_magnetic:
                true_magnetic = 'T'
            else:
                true_magnetic = 'M'

            tm_cog = self.cog_smoothed + settings.magnetic_declination  # Adds 0.0 if True
            cog_deg = degrees(tm_cog)
            cog_deg = int(cog_deg) % 360

            # [k,v for k,v in units.CARDINAL if units.CARDINAL[k] < cog_deg > units.CARDINAL[k] + 22.5]

            cardinal_index = (cog_deg + 11.25) // 22.5
            cardinal_index = abs(cardinal_index)  # Required?
            cardinal_index = int(cardinal_index)
            __, direction = units.CARDINAL[cardinal_index]
            pretty_cog = '{0}°{1} {2} '.format(cog_deg, true_magnetic, direction)

        pub.sendMessage('show_course', show_course=pretty_cog)
        print 'Course: {0} with {1:.0f}° Dec'.format(pretty_cog, settings.magnetic_declination_degrees)  # Test
        return

    def showminmax(self):
        """
        Generates Min, Max and Average from smoothed speed
        """
        if not self.sog_smoothed:
            return

        pretty_minmax = 'No Data'

        if MINAVGMAX[0] == 0.0:
            MINAVGMAX[0] = self.sog_smoothed
        if self.sog_smoothed < MINAVGMAX[0]:
            MINAVGMAX[0] = self.sog_smoothed
        if self.sog_smoothed > MINAVGMAX[2]:
            MINAVGMAX[2] = self.sog_smoothed

        try:
            # 1 hour running average of smoothed sog
            AVERAGEQUE.append(self.sog_smoothed)

            # reduce(lambda a, b: a+b,speed) = (((((a + b) + c) + d + e) + ...)
            # Filter 'None' and add '0' in case of empty queue at startup or thermonuclear war.
            average_sum = reduce(lambda a, b: a + b, filter(None, AVERAGEQUE), 0)
            average_running_sog = average_sum / len(filter(None, AVERAGEQUE))  # Filter neccessary?
            MINAVGMAX[1] = average_running_sog

            _min, _avg, _max = [speeddata * units.SPEED[settings.speed_units] for speeddata in MINAVGMAX]

            pretty_minmax = '{0:02.2f} / {1:02.2f} / {2:02.2f} {3}'.format(_min, _avg, _max, settings.speed_units)

        except Exception as error:
            print 'Minmax error: ', error
            pretty_minmax = 'Collecting Data'  # Indivisable
        finally:
            pub.sendMessage('show_minmax', show_minmax=pretty_minmax)
            print 'Min-Max: ', pretty_minmax  # Test
            return


speedtrack = SpeedTrack()
