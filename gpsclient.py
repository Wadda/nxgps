#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""GPSD client to connect, read, and create gps attributes"""

from gps import time, gps, gpsdata
import socket
import sys
import threading
from pubsub import pub


class GpsClient(threading.Thread):
    """ Connects to GPSD. Publishes streamed data until it dies."""

    def __init__(self):
        try:
            threading.Thread.__init__(self)
            # noinspection PyCallingNonCallable
            self.gpsd = gps(mode=0x000001)  # starting the stream of info
            self.running = False
        except socket.error as error:  # TODO: keep alive for reconnection
            print "GPSD ERROR CHECK GPS CONNECTION\n" * 5
            print 'This is what happened...'
            print error
            sys.exit()

    def run(self):
        self.running = True
        while self.running:
            # grab EACH set of gpsd info to clear the buffer
            self.gpsd.next()

    def stopclient(self):
        """Stops Client"""
        self.running = False

    @property
    def fix(self):
        """Creates instance attributes
        gpsdata.fix.altitude   # 41.9 (meters)
        gpsdata.fix.climb      # 0.1 (m/s) ## Error Estimates 95% confidence
        gpsdata.fix.epc        # nan (not a number) Climb/sink error estimate m/s
        gpsdata.fix.epd        # nan (cheap gps) Direction error estimate
        gpsdata.fix.eps        # 6.12 (m/s) Speed error estimate
        gpsdata.fix.ept        # 0.005 (seconds) Time error estimate
        gpsdata.fix.epv        # 5.175 (meters) Vertical error estimate
        gpsdata.fix.epx        # 2.345 (meters) Latitude error estimate
        gpsdata.fix.epy        # 3.456 (meters) Longitude error estimate
        gpsdata.fix.latitude   # -33.123456789
        gpsdata.fix.longitude  # 151.123456789
        gpsdata.fix.mode       # 1|2|3 (1:no fix; 2:2D; 3:3D)
        gpsdata.fix.speed      # 0.123 (m/s)
        gpsdata.fix.time       # u'2013-12-16T05:40:44.000Z'| 1387172437.0 (epoch)
        gpsdata.fix.track      # 203.45 (degrees True North)
        """
        return self.gpsd.fix

    @property
    def utc(self):
        """Creates instance attribute data.utc # u'2013-12-16T05:40:44.000Z'"""
        # pub.sendMessage('utc',data=Gpsclient.utc)
        return self.gpsd.utc

    @property
    def satellites(self):
        """Creates instance tuple gpsdata.satellites # (8, 12) 8 satellites used 12 in view"""
        return self.gpsd.satellites_used, len(self.gpsd.satellites)  # _used


def senddata():
    """ Send the following chunks """

    pub.sendMessage('utc',
                    utc=data.utc)

    pub.sendMessage('lat_lon',
                    lat=data.fix.latitude,
                    lon=data.fix.longitude)

    pub.sendMessage('speed_track',
                    sog=data.fix.speed,
                    cog=data.fix.track)

    pub.sendMessage('satstats',
                    mode=data.fix.mode,
                    usedinview=data.satellites)
    time.sleep(.95)


data = GpsClient()
data.start()

if __name__ == '__main__':
    import os

    gpsdata = GpsClient()
    try:
        # start client
        gpsdata.start()
        while True:
            os.system('clear')  # clear the terminal (optional)
            print "latitude ", gpsdata.fix.latitude
            print "longitude ", gpsdata.fix.longitude
            print "UTC & time ", gpsdata.utc, ' ', gpsdata.fix.time
            print "altitude (m)", gpsdata.fix.altitude
            print "eps ", gpsdata.fix.eps
            print "epx ", gpsdata.fix.epx
            print "epv ", gpsdata.fix.epv
            print "ept ", gpsdata.gpsd.fix.ept
            print "speed (m/s) ", gpsdata.fix.speed
            print "climb (m/s)", gpsdata.fix.climb
            print "track ", gpsdata.fix.track
            print "mode ", gpsdata.fix.mode
            print "sats ", gpsdata.satellites
            time.sleep(.9)

    # Ctrl C
    except KeyboardInterrupt:
        print "User cancelled"

    # Error
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise

    finally:
        print "Stopping gps client"
        gpsdata.stopclient()
        # wait for the tread to finish
        gpsdata.join()

    print "Done"
