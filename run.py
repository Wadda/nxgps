#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'wadda'
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""The tie that binds"""

import gpsclient
import latlon
import odometer
import sogcog
import timestatus
import waypoint
import control

settings = control.settings
units = control.units


def run():
    """calls instance and runs until it doesn't"""
    print 'Fired up------------------------'
    try:
        settings.read_configuration()
    except Exception as error:
        print 'this error in run.py: ', error
        settings.write_configuration()

    while gpsclient.data.running:
        gpsclient.senddata()


if __name__ == '__main__':
    try:
        run()

    # Ctrl C
    except KeyboardInterrupt:
        # print 'Run error is:', error
        print "User cancelled"

    finally:
        print "Stopping gps client"
        gpsclient.data.stopclient()
        # wait for the tread to finish
        gpsclient.data.join()

        print "Done"
